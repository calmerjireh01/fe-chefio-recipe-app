import 'package:fe_chefio_recipe_app/font_style.dart';
import 'package:fe_chefio_recipe_app/home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => new _SignInState();
}

class _SignInState extends State<SignIn>{
  bool show = false;
  bool userExists=true;
  void toggleVisibility() {
    setState(() {
      show = !show;
    });
  }
  void toogleExists() {
    setState(() {
      userExists = !userExists;
    });
  }
  @override
  Widget build(BuildContext context) {

    final emailController= new TextEditingController();
    final passwordController= new TextEditingController();
    final _auth = FirebaseAuth.instance;

    void _navigateToHome(BuildContext context) {
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => Home()));
    }

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(top: 0, left: 24, right: 24, bottom: 75),
            padding: EdgeInsets.only(top: 107),
            child: Column(
              children: [
                SizedBox(
                  height: 32,
                  child: Text(
                    "Welcome back!",
                    style: mainText,
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                SizedBox(
                  height: 25,
                  child: Text(
                    "Please enter your account here",
                    style: secondaryText,
                  ),
                ),
                SizedBox(height: 32,),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 4),
                  decoration: BoxDecoration(
                    border: Border.all(color: Color(0xffD0DBEA)),
                    borderRadius: BorderRadius.circular(32),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.mail_outline_rounded,
                        color: Color(0xff2E3E5C),
                      ),
                      SizedBox(width: 10,),
                      Flexible(
                        child: TextFormField(
                          controller: emailController,
                          decoration: InputDecoration(
                            constraints: BoxConstraints(
                              maxHeight: 56,
                              minWidth: double.infinity
                          ),

                            border: InputBorder.none,
                            hintText: "Email or phone number",
                            hintStyle: secondaryText,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                /*TextFormField(
                  decoration: InputDecoration(
                    constraints: BoxConstraints(
                      maxHeight: 56,
                      minWidth: double.infinity
                    ),
                    contentPadding: EdgeInsets.symmetric(vertical: 16, horizontal: 24),
                    prefixIcon: Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Icon(Icons.mail_rounded),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(32),
                      borderSide: BorderSide(
                        color: Color(0xffD0DBEA)
                      ),
                    ),
                    hintText: "Email or phone number",
                    hintStyle: secondaryText,
                  ),
                  onSaved: (value) {},
                ),
                Container(
                  // margin: EdgeInsets.symmetric(horizontal: 24),
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                  decoration: BoxDecoration(
                    border: Border.all(color: Color(0xffD0DBEA)),
                    borderRadius: BorderRadius.circular(32),
                  ),
                  child: Row(
                    children: [
                      Image.asset("assets/images/Message.png"),
                      SizedBox(width: 10,),
                      Text(
                        "Email or phone number",
                        style: secondaryText,
                      )
                    ],
                  ),
                ),*/
                SizedBox(
                  height: 16,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 4),
                  decoration: BoxDecoration(
                    border: Border.all(color: Color(0xffD0DBEA)),
                    borderRadius: BorderRadius.circular(32),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.lock_outline_rounded,
                            color: Color(0xff2E3E5C),
                          ),
                          SizedBox(width: 10,),
                        ],
                      ),

                      Flexible(
                        child: TextFormField(
                          obscureText: !show,
                          controller: passwordController,
                          decoration: InputDecoration(
                            constraints: BoxConstraints(
                                maxHeight: 56,
                                minWidth: double.infinity
                            ),

                            border: InputBorder.none,
                            hintText: "Password",
                            hintStyle: secondaryText,
                          ),
                        ),
                      ),
                      IconButton(
                        icon: show == false ? Icon(Icons.remove_red_eye_outlined, color: Color(0xff2E3E5C),) : Icon(Icons.visibility_off_outlined, color: Color(0xff2E3E5C),),
                        onPressed: () => toggleVisibility(),
                      ),
                    ],
                  ),
                ),
                /*Container(
                  // margin: EdgeInsets.symmetric(horizontal: 24),
                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                  decoration: BoxDecoration(
                    border: Border.all(color: Color(0xffD0DBEA)),
                    borderRadius: BorderRadius.circular(32),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Image.asset("assets/images/lock.png"),
                          SizedBox(width: 10,),
                          Text(
                            "Password",
                            style: secondaryText,
                          )
                        ],
                      ),
                      Image.asset(
                        "assets/images/Show.png",
                        width: 24,
                        height: 24,
                      ),
                    ],
                  ),
                ),*/
                SizedBox(
                  height: 16,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: () {},
                      child: Text(
                        "Forgot password?",
                        style: subMainText,
                      ),
                    ),
                  ],
                ),
                userExists ?
                SizedBox(
                  height: 62,
                ):
                Column(
                  children: [
                    SizedBox(height: 20,),
                    Text("Sign In Gagal!",
                    style: warningText,),
                    SizedBox(height: 40,),
                  ],
                ),
                Container(
                  child: MaterialButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(32),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 19),
                    minWidth: double.infinity,
                    color: Color(0xff1FCC79),
                    onPressed: () async {
                      try{
                        final user= await _auth.signInWithEmailAndPassword(email: emailController.text, password: passwordController.text);
                        _navigateToHome(context);
                      }
                      catch(e){
                        toogleExists();
                      }
                    },
                    child: Text(
                      "Login",
                      style: buttonText,
                    ),
                  ),
                ),

                Container(
                  margin: EdgeInsets.symmetric(vertical: 24),
                  child: Text("Or continue with", style: secondaryText,),
                ),

                Container(
                  child: MaterialButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(32),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 19),
                    minWidth: double.infinity,
                    color: Color(0xffFF5842),
                    onPressed: () {},
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset("assets/images/Google.png"),
                        SizedBox(width: 4,),
                        Text(
                          "Google",
                          style: buttonText,
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 24,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Don’t have any account?",
                      style: subMainText,
                    ),
                    TextButton(
                      onPressed: () {},
                      child: Text("Sign Up", style: subPrimaryText,)
                    )
                  ],
                ),

              ],
            ),
          ),
        ),
      ),
    );


  }
}