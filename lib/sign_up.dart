import 'package:fe_chefio_recipe_app/themes/colors.dart';
import 'package:fe_chefio_recipe_app/themes/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _formKey= GlobalKey<FormState>();
  final _auth = FirebaseAuth.instance;
  final emailController= new TextEditingController();
  final passwordController= new TextEditingController();
  bool lengthIsTrue=false;
  bool hasNumbers=false;
  bool isVisible=false;
  late String firstInt='';
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 24, right:24, top: 107, bottom: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Column(
                    children: [
                      Text(
                        "Welcome!",
                        style: header,
                      ),
                      SizedBox(height: 8,),
                      Text(
                        "Please enter your account here",
                        style: subHeader,
                      ),
                      SizedBox(height: 32,),
                      TextFormField(
                        controller: emailController,
                        decoration: InputDecoration(
                          hintText: "Email or phone number",
                          hintStyle: subHeader,
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(top: 16,bottom: 16,left: 24,right: 10),
                           child: Icon(Icons.mail_outline_rounded,color: Color(
                               0xFF000000),),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32),
                            borderSide: BorderSide(color: primary)
                          ),
                        ),
                      ),
                      SizedBox(height: 16,),
                      TextFormField(
                        controller: passwordController,
                        onChanged: (value){
                          if(value.length!=0){
                            print(value);
                            if(value.length>=8){
                              setState(() {
                                this.lengthIsTrue=true;
                              });
                            }
                            else{
                              setState(() {
                                this.lengthIsTrue=false;
                              });
                            }
                            if(double.tryParse(value.substring(value.length-1,value.length))!=null&&this.hasNumbers==false){
                              setState(() {
                                this.firstInt=value.substring(value.length-1,value.length);
                              });
                            }
                            if(this.firstInt!=''){
                              if(value.toString().contains(this.firstInt)){
                                setState(() {
                                  this.hasNumbers=true;
                                });
                              }
                              else{
                                setState(() {
                                  this.firstInt='';
                                  this.hasNumbers=false;
                                });
                              }
                            }
                          }
                        },
                        obscureText: isVisible?false:true,
                        decoration: InputDecoration(

                          hintText: "Password",
                          hintStyle: subHeader,
                          suffixIcon: Padding(
                            padding: EdgeInsets.only(top: 16,bottom: 16,right: 16),
                            child: GestureDetector(
                              onTap: (){
                                setState(() {
                                  isVisible=!isVisible;
                                });
                              },
                              child: Icon(Icons.remove_red_eye_outlined,color: isVisible?Colors.black:secondaryText,),
                            ),
                          ),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(top: 16,bottom: 16,left: 24,right: 10),
                            child: Icon(Icons.lock_open_outlined,color: Color(
                                0xFF000000),),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32),
                          ),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32),
                              borderSide: BorderSide(color: primary)
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 54,),
                Text(
                  "Your Password must contain:",
                  style: paragraph1,
                ),
                SizedBox(
                  height: 16,
                ),
                Row(
                  children: [
                    Icon(Icons.check_circle_sharp,color: lengthIsTrue?primary:Colors.black,),
                    SizedBox(width: 8,),
                    Text(
                      "Atleast 6 characters",
                      style: lengthIsTrue?paragraph2:subHeader,
                    )
                  ],
                ),
                SizedBox(
                  height: 16,
                ),
                Row(
                  children: [
                    Icon(Icons.check_circle_sharp,color: hasNumbers?primary:Colors.black,),
                    SizedBox(width: 8,),
                    Text(
                      "Contains a number",
                      style: hasNumbers?paragraph2:subHeader,
                    )
                  ],
                ),
                SizedBox(
                  height: 72,
                ),
                RawMaterialButton(
                  onPressed: () async {
                    final user= await _auth.createUserWithEmailAndPassword(email: this.emailController.text, password: this.passwordController.text);
                    print(user);
                    },
                  constraints: BoxConstraints(
                    minWidth: double.infinity,
                  ),
                  fillColor: primary,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 19),
                    child: Text(
                      "Sign Up",
                      style: buttonText,
                    ),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(32)
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
