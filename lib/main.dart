import 'package:fe_chefio_recipe_app/onboarding.dart';
import 'package:fe_chefio_recipe_app/sign_in.dart';
import 'package:fe_chefio_recipe_app/sign_up.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return FutureBuilder(
      future: Firebase.initializeApp(),
      builder: (context, snapshot) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          routes: {

            '/': (context) => SignIn(),
            '/SignIn': (context) => SignIn(),

          },
        );
      }
    );
  }
}

