import 'package:fe_chefio_recipe_app/font_style.dart';
import 'package:fe_chefio_recipe_app/sign_in.dart';
import 'package:flutter/material.dart';

class Onboarding extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    void _navigateToNextScreen(BuildContext context) {
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => SignIn()));
    }

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 12,),

              Image.asset("assets/images/Onboarding.png"),

              SizedBox(height: 48),
              Text(
                "Start Cooking",
                style: mainText,
              ),
              SizedBox(height: 16),
              Text(
                "Let’s join our community\nto cook better food!",
                style: secondaryText,
                textAlign: TextAlign.center,
              ),

              Container(
                padding: EdgeInsets.only(top: 72),
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: MaterialButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(32),
                  ),
                  padding: EdgeInsets.symmetric(vertical: 19),
                  minWidth: double.infinity,
                  color: Color(0xff1FCC79),
                  onPressed: () {
                    _navigateToNextScreen(context);
                  },
                  child: Text(
                    "Get Started",
                    style: buttonText,
                  ),
                ),
              ),

              SizedBox(height: 72,),
            ],
          ),
        ),
      ),
    );
  }

}