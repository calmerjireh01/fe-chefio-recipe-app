import 'package:fe_chefio_recipe_app/themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

TextStyle mainText = TextStyle(
  fontFamily: "Inter",
  fontWeight: FontWeight.w700,
  fontSize: 22,
  color: Color(0xff2E3E5C),
);

TextStyle smallerMainText = TextStyle(
  fontFamily: "Inter",
  fontWeight: FontWeight.w700,
  fontSize: 17,
  color: Color(0xff2E3E5C),
);

TextStyle subMainText = TextStyle(
  fontFamily: "Inter",
  fontWeight: FontWeight.w500,
  fontSize: 15,
  color: Color(0xff2E3E5C),
);

TextStyle subPrimaryText = TextStyle(
  fontFamily: "Inter",
  fontWeight: FontWeight.w700,
  fontSize: 15,
  color: Color(0xff1FCC79),
);

TextStyle secondaryText = TextStyle(
  fontFamily: "Inter",
  fontWeight: FontWeight.w500,
  fontSize: 17,
  color: Color(0xff9FA5C0),
);

TextStyle buttonText = TextStyle(
  fontFamily: "Inter",
  fontWeight: FontWeight.w700,
  fontSize: 15,
  color: Colors.white,
);

<<<<<<< HEAD
TextStyle categoryMainText = TextStyle(
  fontFamily: "Inter",
  fontWeight: FontWeight.w600,
  fontSize: 15,
  color: Color(0xff2E3E5C),
);

TextStyle categorySubText = TextStyle(
  fontFamily: "Inter",
  fontWeight: FontWeight.w600,
  fontSize: 15,
  color: Color(0xff9FA5C0),
);

TextStyle recipeProfileText = TextStyle(
  fontFamily: "Inter",
  fontWeight: FontWeight.w500,
  fontSize: 12,
  color: Color(0xff2E3E5C),
);

TextStyle recipeDescText = TextStyle(
  fontFamily: "Inter",
  fontWeight: FontWeight.w500,
  fontSize: 12,
  color: Color(0xff9FA5C0),
);


TextStyle warningText= GoogleFonts.inter(
  fontSize: 15,
  color: secondary,
  fontWeight: FontWeight.w600,
);


