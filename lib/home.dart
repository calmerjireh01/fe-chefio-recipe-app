import 'package:fe_chefio_recipe_app/font_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home>{
  int choice = 0;

  void change_choice(int i) {
    setState(() {
      choice = i;
    });
  }

  @override
  Widget build(BuildContext context) {

    Widget recipe(String profileUrl, String profileName, String recipeUrl, String recipeName, String category, String duration) {
      return Container(
        margin: EdgeInsets.only(top: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Image.asset(
                    profileUrl,
                    width: 30,
                    height: 30,
                  ),
                  SizedBox(width: 8,),
                  Text(
                    profileName,
                    style: recipeProfileText,
                  ),
                ],
              ),
              SizedBox(height: 16,),
              Image.asset(
                recipeUrl,
                width: 150,
                height: 150,
              ),
              SizedBox(height: 16,),
              Text(
                recipeName,
                style: smallerMainText,
              ),
              SizedBox(height: 8,),
              Row(
                children: [
                  Text(
                    category,
                    style: recipeDescText,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Icon(Icons.circle, size: 5, color: Color(0xff9FA5C0),),
                  ),
                  Text(
                    duration,
                    style: recipeDescText,
                  )
                ],
              ),
            ],
          ),
        );
    }

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(top: 16),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 24),
                        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                        decoration: BoxDecoration(
                          color: Color(0xffF4F5F7),
                          borderRadius: BorderRadius.circular(32),
                        ),
                        child: Row(
                          children: [
                            Icon(CupertinoIcons.search),
                            SizedBox(width: 8,),
                            Text(
                              "Search",
                              style: secondaryText,
                            ),
                          ],
                        ),
                      ),
                      Text(
                        "Category",
                        style: smallerMainText,
                      ),
                      SizedBox(height: 16,),
                      Row(
                        children: [
                          Container(
                            width: 68,
                            height: 48,
                            child: MaterialButton(
                              elevation: 0,
                              hoverElevation: 0,
                              focusElevation: 0,
                              highlightElevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(32),
                              ),
                              padding: EdgeInsets.symmetric(vertical: 15),

                              color: choice == 0 ? Color(0xff1FCC79) : Color(0xffF4F5F7),
                              onPressed: () {
                                change_choice(0);
                              },
                              child: Text(
                                "All",
                                style: choice == 0 ? buttonText : secondaryText,
                              ),
                            ),
                          ),
                          SizedBox(width: 16,),
                          Container(
                            width: 86,
                            height: 47,
                            child: MaterialButton(
                              elevation: 0,
                              hoverElevation: 0,
                              focusElevation: 0,
                              highlightElevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(32),
                              ),
                              padding: EdgeInsets.symmetric(vertical: 15),

                              color: choice == 1 ? Color(0xff1FCC79) : Color(0xffF4F5F7),
                              onPressed: () {
                                change_choice(1);
                              },
                              child: Text(
                                "Food",
                                style: choice == 1 ? buttonText : secondaryText,
                              ),
                            ),
                          ),
                          SizedBox(width: 16,),
                          Container(
                            width: 86,
                            height: 47,
                            child: MaterialButton(
                              elevation: 0,
                              hoverElevation: 0,
                              focusElevation: 0,
                              highlightElevation: 0,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(32),
                              ),
                              padding: EdgeInsets.symmetric(vertical: 15),

                              color: choice == 2 ? Color(0xff1FCC79) : Color(0xffF4F5F7),
                              onPressed: () {
                                change_choice(2);
                              },
                              child: Text(
                                "Drink",
                                style: choice == 2 ? buttonText : secondaryText,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 23),

                    ],
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  height: 8,
                  child: DecoratedBox(
                    decoration: BoxDecoration(

                      color: Color(0xffF4F5F7)
                    ),
                  ),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      //width: double.infinity,
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            width: 3,
                            color: Color(0xff1FCC79),

                          )
                        )
                      ),
                      padding: EdgeInsets.symmetric(vertical: 24, horizontal: 75),
                      child: Text(
                        "All",
                        style: categoryMainText
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 24, horizontal: 75),
                      child: Text(
                        "Saved",
                        style: categorySubText,
                      ),
                    ),
                  ],
                ),
                Container(
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          recipe("assets/images/profile1.png", "Calum Lewis", "assets/images/pancake.png", "Pancake", "Food", ">60 mins"),
                          recipe("assets/images/profile1.png", "Calum Lewis", "assets/images/pancake.png", "Pancake", "Food", ">60 mins"),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          recipe("assets/images/profile1.png", "Calum Lewis", "assets/images/pancake.png", "Pancake", "Food", ">60 mins"),
                          recipe("assets/images/profile1.png", "Calum Lewis", "assets/images/pancake.png", "Pancake", "Food", ">60 mins"),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          recipe("assets/images/profile1.png", "Calum Lewis", "assets/images/pancake.png", "Pancake", "Food", ">60 mins"),
                          recipe("assets/images/profile1.png", "Calum Lewis", "assets/images/pancake.png", "Pancake", "Food", ">60 mins"),
                        ],
                      ),
                      SizedBox(height: 24,)
                    ],
                  ),
                ),

              ],
            ),
          ),
        ),
      ),

      bottomNavigationBar: Container(
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          unselectedItemColor: Color(0xff9FA5C0),
          selectedItemColor: Color(0xff1FCC79),
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.home_filled), label: "Home"),
            BottomNavigationBarItem(icon: Icon(Icons.edit), label: "Upload"),
            BottomNavigationBarItem(icon: Icon(Icons.qr_code_scanner), label: "Scan"),
            BottomNavigationBarItem(icon: Icon(Icons.notifications), label: "Notification"),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: "Profile"),
          ],
        ),
      ),
    );
  }

}