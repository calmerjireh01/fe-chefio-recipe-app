import 'package:fe_chefio_recipe_app/themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

TextStyle header= GoogleFonts.inter(
  fontSize: 22,
  fontWeight: FontWeight.w700
);

TextStyle subHeader= GoogleFonts.inter(
    fontSize: 15,
    color: secondaryText,
    fontWeight: FontWeight.w600
);

TextStyle paragraph1=GoogleFonts.inter(
  fontSize: 17,
  fontWeight: FontWeight.w600
);

TextStyle paragraph2=GoogleFonts.inter(
    fontSize: 15,
    fontWeight: FontWeight.w500
);

TextStyle buttonText =GoogleFonts.inter(
    fontSize: 15,
    color: form,
    fontWeight: FontWeight.w700,
);




