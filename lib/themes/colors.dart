import 'package:flutter/material.dart';

Color primary= const Color(0xff1FCC79);

Color secondary= const Color(0xffFF6464);

Color mainText= const Color(0xff2E3E5C);

Color secondaryText= const Color(0xff9FA5C0);

Color outline= const Color(0xffD0DBEA);

Color form= const Color(0xffF4F5F7);

